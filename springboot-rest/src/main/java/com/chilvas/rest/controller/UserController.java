package com.chilvas.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.chilvas.entities.User;
import com.chilvas.repositories.UserRepository;
import com.chilvas.rest.exception.ResourceNotFoundException;
//@CrossOrigin
@RestController
public class UserController {
	@Autowired
	private UserRepository userRepository;

	@CrossOrigin
//	@CrossOrigin(origins= {"http://domain1.com","http://domain2.com"},
//			allowedHeaders="X-AUTH-TOKEN",
//			allowCredentials="false",
//			maxAge=15*60,
//			methods= {RequestMethod.GET,RequestMethod.POST}
//	)
	@GetMapping("/users/{id}")
	public User getUser(@PathVariable("id") Integer id) {
		return userRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Not User found with id=" + id));
	}

	@DeleteMapping("{id}")
	public void deletePost(@PathVariable("id") Integer id) {
		User user = userRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("No User found with id=" + id));
		userRepository.delete(user);
	}
}
