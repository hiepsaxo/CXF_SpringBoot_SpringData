package com.chilvas.rest.controller;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.chilvas.entities.Comment;
import com.chilvas.entities.Post;
import com.chilvas.repositories.CommentRepository;
import com.chilvas.repositories.PostRepository;
import com.chilvas.rest.exception.PostDeletionException;
import com.chilvas.rest.exception.ResourceNotFoundException;
import com.chilvas.utils.ErrorDetails;

@RestController
@RequestMapping(value = "/posts")
public class PostController {
	@Autowired
	PostRepository postRepository;
	@Autowired
	CommentRepository commentRepository;

	@ExceptionHandler(PostDeletionException.class)
	public ResponseEntity<?> servletRequestBindingException(PostDeletionException e) {
		ErrorDetails errorDetails = new ErrorDetails();
		errorDetails.setErrorMessage(e.getMessage());
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		errorDetails.setDevErrorMessage(sw.toString());
		return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@PostMapping(value = "")
	public ResponseEntity<Post> createPost(@RequestBody @Valid Post post, BindingResult result) {
		if (result.hasErrors()) {
			return new ResponseEntity<>(post, HttpStatus.BAD_REQUEST);
		}
		Post savedPost = postRepository.save(post);
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set("MyResponseHeader1", "MyValue1");
		responseHeaders.set("MyResponseHeader2", "MyValue2");
		return new ResponseEntity<Post>(savedPost, responseHeaders, HttpStatus.CREATED);
	}

	@GetMapping("")
	public List<Post> listPosts() {
		return postRepository.findAll();
	}

	@GetMapping(value = "/{id}")
	public Post getPost(@PathVariable("id") Integer id) {
		return postRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("No post found with id=" + id));
	}

	@PutMapping("/{id}")
	public Post updatePost(@PathVariable("id") Integer id, @RequestBody Post post) {
		postRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("No post found with id=" + id));
		return postRepository.save(post);
	}

	@DeleteMapping("{id}")
	public void deletePost(@PathVariable("id") Integer id) {
		Post post = postRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("No post found with id=" + id));
		try {
			postRepository.deleteById(post.getId());
		} catch (Exception e) {
			throw new PostDeletionException("Post with id=" + id + " can't be deleted.");
		}
	}

	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping("{id}/comments")
	public void createPostComment(@PathVariable("id") Integer id, @RequestBody Comment comment) {
		Post post = postRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("No post found with id=" + id));
		post.getComments().add(comment);
	}

	@DeleteMapping("/{postId}/comments/{commentId}")
	public void deletePostComment(@PathVariable("postId") Integer postId,
			@PathVariable("commentId") Integer commentId) {
		commentRepository.deleteById(commentId);
	}
}
