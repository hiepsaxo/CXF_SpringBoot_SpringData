package com.chilvas.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.chilvas.entities.Post;

public interface PostRepository extends JpaRepository<Post, Integer>{

}
