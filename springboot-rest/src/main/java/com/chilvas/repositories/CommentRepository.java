package com.chilvas.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.chilvas.entities.Comment;

public interface CommentRepository extends JpaRepository<Comment, Integer>{

}
