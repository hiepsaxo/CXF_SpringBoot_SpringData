package com.chilvas.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.chilvas.entities.User;

public interface UserRepository extends JpaRepository<User, Integer>{

}
