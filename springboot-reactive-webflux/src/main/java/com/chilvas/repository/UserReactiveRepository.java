package com.chilvas.repository;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import com.chilvas.document.User;

public interface UserReactiveRepository extends ReactiveCrudRepository<User, String>
{
}