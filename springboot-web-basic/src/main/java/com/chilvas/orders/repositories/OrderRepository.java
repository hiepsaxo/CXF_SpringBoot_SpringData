package com.chilvas.orders.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.chilvas.orders.entities.Order;

public interface OrderRepository extends JpaRepository<Order, Integer>{

}
