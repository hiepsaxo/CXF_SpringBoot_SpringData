package com.chilvas.jpa.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.chilvas.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>{
	@Query("SELECT u FROM User u WHERE u.name = ?1")
	User findByName(String name);
	@Query("SELECT u FROM User u WHERE u.name LIKE %?1%")
	List<User> findByNameLike(String name);
}
