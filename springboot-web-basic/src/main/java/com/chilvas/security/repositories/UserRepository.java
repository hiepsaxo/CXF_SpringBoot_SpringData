package com.chilvas.security.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.chilvas.security.entities.User;

public interface UserRepository extends JpaRepository<User, Integer>{
	
}
