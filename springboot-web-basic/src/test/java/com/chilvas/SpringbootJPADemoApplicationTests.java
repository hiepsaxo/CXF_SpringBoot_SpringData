package com.chilvas;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Optional;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.test.context.junit4.SpringRunner;

import com.chilvas.entity.User;
import com.chilvas.jpa.service.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootJPADemoApplicationTests {

	@Autowired
	private UserRepository userRepository;

	@Test
	public void findAllUsers() {
		List<User> users = userRepository.findAll();
		assertNotNull(users);
		assertTrue(!users.isEmpty());
	}

	@Test
	@Ignore
	public void createUser() {
		User user = new User();
		user.setEmail("hiepnguyen@gmail.com");
		user.setName("hiep");
		user.setDisabled(false);
		User savedUser = userRepository.save(user);
		User newUser = userRepository.findById(savedUser.getId()).get();
		System.out.println(newUser);
		assertEquals("hiep", newUser.getName());
		assertEquals("hiepnguyen@gmail.com", newUser.getEmail());
	}

	@Test
	public void findUserById() {
		Optional<User> user = userRepository.findById(1);
		assertNotNull(user.get());
	}

	@Test
	public void findUserByName() {
		User user = userRepository.findByName("hiep");
		assertNotNull(user);
	}

	@Test
	public void findUsersByNameLike() {
		List<User> users = userRepository.findByNameLike("h");
		assertTrue(!users.isEmpty());
	}

	@Test
	public void sortUsers() {
		Sort sort = new Sort(Direction.ASC, "name");
		List<User> sortedUsers = userRepository.findAll(sort);
		List<User> users = userRepository.findAll();
		int size = sortedUsers.size();
		for (int i = 0; i < size; i++) {
			assertTrue(sortedUsers.get(i).getId() == users.get(i).getId());
		}
	}
}
