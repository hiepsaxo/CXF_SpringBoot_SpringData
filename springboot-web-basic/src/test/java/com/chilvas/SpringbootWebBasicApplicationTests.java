package com.chilvas;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.chilvas.model.User;
import com.chilvas.repository.UserRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringbootWebBasicApplicationTests {
	@Autowired
	private UserRepository userRepository;
	
	@Test
	public void findAllUsers(){
		List<User> users = userRepository.findAll();
		assertNotNull(users);
		assertTrue(!users.isEmpty());
	}

	@Test
	public void findUserById(){
		User user = userRepository.findUserById(1);
		assertNotNull(user);
	}
	
	@Test
	public void createUser(){
		User user = new User();
		user.setEmail("hiep@gmail.com");
		user.setUsername("hiep");
		User savedUser = userRepository.create(user);
		User newUser = userRepository.findUserById(savedUser.getId());
		assertNotNull(newUser);
		assertEquals("hiep", newUser.getUsername());
		assertEquals("hiep@gmail.com", newUser.getEmail());
	}
}
