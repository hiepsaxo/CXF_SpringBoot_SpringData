package com.chilvas;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.chilvas.orders.entities.Order;
import com.chilvas.orders.repositories.OrderRepository;
import com.chilvas.security.entities.User;
import com.chilvas.security.repositories.UserRepository;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=SpringbootMultipleDSDemoApplication.class)
@SpringBootTest
public class SpringbootMultipleDSDemoApplicationTests {
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private OrderRepository orderRepository;

	@Test
	public void findAllUsers() {
		List<User> users = userRepository.findAll();
		assertNotNull(users);
		assertTrue(!users.isEmpty());
	}

	@Test
	public void findAllOrders() {
		List<Order> orders = orderRepository.findAll();
		assertNotNull(orders);
		assertTrue(!orders.isEmpty());
	}
}
