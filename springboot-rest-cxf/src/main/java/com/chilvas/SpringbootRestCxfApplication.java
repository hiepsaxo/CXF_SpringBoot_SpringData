package com.chilvas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.chilvas.config.AppConfig;

@SpringBootApplication
public class SpringbootRestCxfApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppConfig.class, args);
	}
}
