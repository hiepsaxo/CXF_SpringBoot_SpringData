package com.chilvas.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.stereotype.Component;

import com.chilvas.bean.Person;

@Path("/people")
@Component
public class PeopleRestService {
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Person getPeople() {
			Person person = new Person();
			person.setId(1);
			person.setName("name 1" );
		return person;
	}
}
