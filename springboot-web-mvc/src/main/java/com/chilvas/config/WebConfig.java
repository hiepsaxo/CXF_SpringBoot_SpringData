package com.chilvas.config;

import javax.servlet.DispatcherType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletListenerRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.chilvas.servlet.MyServlet;

import net.bull.javamelody.MonitoringFilter;
import net.bull.javamelody.SessionListener;

@Configuration
public class WebConfig implements WebMvcConfigurer {

	@Autowired
	private MyServlet myServlet;

	@Bean
	public ServletRegistrationBean<MyServlet> myServletBean() {
		ServletRegistrationBean<MyServlet> servlet = new ServletRegistrationBean<>();
		servlet.setServlet(myServlet);
		servlet.addUrlMappings("/myServlet");
		return servlet;
	}
	@Bean(name="javamelodyFilter")
	public FilterRegistrationBean<MonitoringFilter> javamelodyFilterBean() {
		FilterRegistrationBean<MonitoringFilter> registration = new FilterRegistrationBean<>();
		registration.setFilter(new MonitoringFilter());
		registration.addUrlPatterns("/*");
		registration.setName("javamelodyFilter");
		registration.setAsyncSupported(true);
		registration.setDispatcherTypes(DispatcherType.REQUEST, DispatcherType.ASYNC);
		
		return registration;
	}
	@Bean(name="javamelodySessionListener")
	public ServletListenerRegistrationBean<SessionListener> sessionListener(){
		return new ServletListenerRegistrationBean<SessionListener>(new SessionListener());
	}
	
	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	@Override
	public void addFormatters(FormatterRegistry registry) {
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/assets/").addResourceLocations("/resources/assets/");
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/login").setViewName("public/login");
		registry.addRedirectViewController("/", "/home");
	}

}
