package com.chilvas.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.chilvas.bean.User;

@Controller
public class RegistrationController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@GetMapping("/registration")
	public String registrationForm(Model model) {
		model.addAttribute("user", new User());
		return "registration";
	}

	@PostMapping("/registration")
	public String handleRegistration(@Valid User user,BindingResult result) {
		logger.info("Registering User: " + user);
		if(result.hasErrors()) {
			return "registration";
		}
		return "redirect:/registrationsuccess";
	}
}
