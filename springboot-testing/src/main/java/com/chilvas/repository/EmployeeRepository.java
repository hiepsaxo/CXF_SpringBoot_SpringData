package com.chilvas.repository;

import java.util.List;

import com.chilvas.bean.Employee;

public interface EmployeeRepository {
	List<Employee> findAllEmployees();
}
