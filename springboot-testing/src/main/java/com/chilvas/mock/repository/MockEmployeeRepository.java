package com.chilvas.mock.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import com.chilvas.bean.Employee;
import com.chilvas.repository.EmployeeRepository;

@Repository
@Profile("test")
public class MockEmployeeRepository implements EmployeeRepository {

	@Override
	public List<Employee> findAllEmployees() {
		List<Employee> employees = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			Employee emp = new Employee();
			emp.setId(i + 1);
			emp.setName("name " + (i + 1));
			emp.setSalary(Double.valueOf(i + 1));

			employees.add(emp);
		}
		return employees;
	}

}
