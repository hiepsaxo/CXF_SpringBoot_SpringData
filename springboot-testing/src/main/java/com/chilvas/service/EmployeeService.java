package com.chilvas.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.chilvas.bean.Employee;
import com.chilvas.repository.EmployeeRepository;

@Service
public class EmployeeService {
	private EmployeeRepository employeeRepository;

	@Autowired
	public EmployeeService(EmployeeRepository employeeRepository) {
		this.employeeRepository = employeeRepository;
	}

	public Employee getMaxSalariedEmployee() {
		Employee emp = null;
		List<Employee> emps = employeeRepository.findAllEmployees();
		// loop through emps and find max salaried emp
		return emp;
	}
}
