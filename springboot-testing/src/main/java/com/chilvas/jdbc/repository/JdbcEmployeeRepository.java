package com.chilvas.jdbc.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.chilvas.bean.Employee;
import com.chilvas.repository.EmployeeRepository;

@Service
@Profile("production")
public class JdbcEmployeeRepository implements EmployeeRepository {
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Employee> findAllEmployees() {
		return null;
	}

}
