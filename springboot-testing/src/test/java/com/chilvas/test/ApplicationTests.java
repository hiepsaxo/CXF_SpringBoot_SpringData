package com.chilvas.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.chilvas.bean.Employee;
import com.chilvas.service.EmployeeService;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {
	@Autowired
	EmployeeService employeeService;
	
	public void test_getMaxSalariedEmployee(){
		Employee employee = employeeService.getMaxSalariedEmployee();
		assertNotNull(employee);
		assertEquals(2, employee.getId().intValue());
		assertEquals("B", employee.getName());
		assertEquals(75000, employee.getSalary().intValue());
	}
}
